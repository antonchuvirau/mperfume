 // Main scripts for the project
 ! function () {
     var isMobile;
     var mCarousel = [];
     var innerCarousel = [];
     var isCompleted = false;
     var filterCarousel;
     var productSizeCarousel;
     var lettersSimpleBar;
     var productCarousel;
     var checkedInputNumber = 0;
     $(document).ready(function () {
         //Phone mask
         $('input[type="tel"]').inputmask( '+375 (99) 999-99-99', { placeholder: ' ', showMaskOnHover: false, clearMaskOnLostFocus: false });
         // Hide Header on on scroll down
         var didScroll;
         var lastScrollTop = 0;
         var delta = 5;
         var navbarHeight = $('.m-header').outerHeight();
         $(window).scroll(function (event) {
             didScroll = true;
         });
         setInterval(function () {
             if (didScroll) {
                 hasScrolled();
                 didScroll = false;
             }
         }, 250);

         function hasScrolled() {
             var st = $(window).scrollTop();
             // Make sure they scroll more than delta
             if (Math.abs(lastScrollTop - st) <= delta)
                 return;
             // If they scrolled down and are past the navbar, add class .nav-up.
             // This is necessary so you never see what is "behind" the navbar.
             if (st > lastScrollTop && st > navbarHeight) {
                 // Scroll Down
                 $('.m-header').removeClass('nav-down').addClass('nav-up');
             } else {
                 // Scroll Up
                 if (st + $(window).height() < $(document).height()) {
                     $('.m-header').removeClass('nav-up').addClass('nav-down');
                 }
             }
             lastScrollTop = st;
         }
         //JQuery
         if ($('.m-home-slider').length !== 0) {
             var mHomeSlider = new Swiper('.m-home-slider__container', {
                 pagination: {
                     el: '.m-home-slider__pagination',
                     type: 'bullets'
                 }
             });
         }
         //Click outside
         $(document).mouseup(function (e) {
             var container = $(".filter-item");
             if (!container.is(e.target) && container.has(e.target).length === 0) {
                 $('.filter-item__wrapper').removeClass('filter-item__wrapper_opened');
                 $('.filter-item__header-wrapper').removeClass('filter-item__header-wrapper_active');
             }
         });
         //To top
         $('.to-top').on('click', function () {
             $('body, html').animate({
                 scrollTop: 0
             }, 1000)
         });
         //Autocomplete
         var autocompleteOptions = {
             data: ["blue", "green", "pink", "red", "yellow"]
         };
         $("#autocomplete").easyAutocomplete(autocompleteOptions);
         $('.m-header__menu-btn').on('click', function () {
             $('.m-menu').toggleClass('m-menu_opened');
             $('html').toggleClass('is-locked');
         });
         var phoneElVisibility = false;
         $('.m-header__phone-btn').on('click', function () {
             if (!phoneElVisibility && $(window).scrollTop() > 50) {
                 $('html, body').animate({
                     scrollTop: 0
                 }, 1000);
             }
             $('.header__middle').find('.row > div:nth-child(2), .row > div:nth-child(3)').slideToggle();
             phoneElVisibility = !phoneElVisibility;
         });
         var searchElVisibility;
         if ($('body').hasClass('home')) {
             searchElVisibility = true;
         } else {
             searchElVisibility = false;
         }
         $('.m-header__search-btn').on('click', function () {
             if (!searchElVisibility && $(window).scrollTop() > 50) {
                 $('html, body').animate({
                     scrollTop: 0
                 }, 1000);
             }
             $('.header__middle').find('.row > div:last-child').slideToggle();
             searchElVisibility = !searchElVisibility;
         });
         if ($('.filter-item__wrapper')) {
             $('.filter-item__wrapper').each(function () {
                 if ($(this).outerHeight() >= 365) {
                     $(this).addClass('filter-item__wrapper_scrolled');
                 }
             });
         }
         if ($('.filter-item')) {
             $('.filter-item__wrapper-btn').on('click', function () {
                 $(this).parent().removeClass('filter-item__wrapper_scrolled');
             });
         }
         if ($('.filter-item')) {
             var filterItemsArray = $('.filter-item');
             var checkedInputsArray = [];
             for (var i = 0; i < filterItemsArray.length; i++) {
                 checkedInputsArray[i] = 0;
             }
             $('.filter-item__list input').on('click', function () {
                 var parentNode = $(this).closest('.filter-item__list');
                 var parentNodeIndex = $('.filter-item__list').index(parentNode);
                 if ($(this).is(':checked')) {
                     checkedInputsArray[parentNodeIndex] += 1;
                     parentNode.closest('.filter-item').find('.filter-item__name span').text(': ' + checkedInputsArray[parentNodeIndex]);
                     checkArrayItemValue(parentNodeIndex, parentNode);
                 } else {
                     checkedInputsArray[parentNodeIndex] -= 1;
                     parentNode.closest('.filter-item').find('.filter-item__name span').text(': ' + checkedInputsArray[parentNodeIndex]);
                     checkArrayItemValue(parentNodeIndex, parentNode);
                 }
             });
             $('.filter-item__reset-btn').on('click', function () {
                 var parentNodeIndex = $('.filter-item__list').index($(this).parent().next().find('.filter-item__list'));
                 checkedInputsArray[parentNodeIndex] = 0;
                 checkArrayItemValue(parentNodeIndex, $(this).parent().next().find('.filter-item__list'));
                 var inputList = $(this).parent().next().find('input');
                 inputList.each(function () {
                     $(this).prop('checked', false);
                 });
             });
         }
         if ($('.product-gallery__carousel')) {
             productCarousel = new Swiper('.product-gallery__carousel', {
                 effect: 'fade',
                 fadeEffect: {
                     crossFade: true
                 },
                 pagination: {
                     el: '.product-gallery__pagination',
                     type: 'bullets',
                     clickable: true
                 }
             });
         }
         $('.product-gallery__thumb').on('click', function () {
             if ($(this).find('img').hasClass('product-gallery__thumb-img_active')) {} else {
                 var imageIndex = $('.product-gallery__thumb-img').index($(this).find('img'));
                 $('.product-gallery__thumb-img').removeClass('product-gallery__thumb-img_active');
                 $(this).find('img').addClass('product-gallery__thumb-img_active');
                 productCarousel.slideTo(imageIndex);
             }
         });
         getDeviceType();
         if (isMobile && !isCompleted) {
             $('.footer__top > .row').prepend($('.footer__bottom > .row > .col-12:last-child .col-lg-6:nth-child(2)'));
             $('.footer__top > .row').prepend($('.footer__bottom > .row > .col-12:last-child .col-lg-6:first-child'));
             $('.footer__top > .row .col-12:nth-child(2)').after($('.footer__bottom > .row > .col-12:first-child'));
             $('.m-menu__grid').append($('.menu__wrapper a'));
             $('.m-menu__grid').append($('.top-menu a'));
             $('.m-menu__grid').append($('.user-option'));
             $('.m-menu__grid a').removeClass('top-menu__link');
             $('.m-menu__grid a').removeClass('menu__link');
             $('.m-menu__grid a').removeClass('menu__link_s');
             $('.m-menu__grid a').addClass('m-menu__link');
             $('.brands__letters-list').addClass('custom-scrollbar');
             lettersSimpleBar = new SimpleBar(document.querySelector('.brands__letters-list.custom-scrollbar'), {
                 autoHide: false
             });
             lettersSimpleBar.getContentElement();
             $('.search__input').attr('placeholder', 'Начните вводить бренд');
             if ($('.product-options')) {
                 $('.product-options').after($('.product-gallery'));
             }
             $('.catalog-template__list .box__col.col-12').each(function (index) {
                 if (index % 2 !== 0) {
                     $(this).next().find('.box__col.col-6:nth-child(4)').after($(this));
                 } else {
                     $(this).prev().find('.box__col.col-6:nth-child(4)').after($(this));
                 }
             });
             $('.header__options').append($('.brands__grid'));
             if ($('.tabs')) {
                 $('.js-tab-section').each(function (index) {
                     $('.js-tab-btn').eq(index).after($(this));
                     $('.js-tab-btn').eq(0).addClass('js-tab-btn_active');
                 });
             }
             if ($('.filter-form__title')) {
                 $('.filter-form__title').on('click', function () {
                     if ($(this).hasClass('filter-form__title_active')) {
                         $(this).removeClass('filter-form__title_active');
                         $(this).next().removeClass('filter-form__wrapper_opened');
                     } else {
                         $(this).addClass('filter-form__title_active');
                         $(this).next().addClass('filter-form__wrapper_opened');
                     }
                 });
             }
             createMSwiperSlider();
             createFilterCarousel();
             createProductSizeCarousel();
             createInnerCarousel();
             if ($('.about-brand__title')) {
                 $('.about-brand > .row > .col-12:first-child').prepend($('.about-brand__title'));
             }
             if ($('.quick-order')) {
                 $('.product-template__submit').after($('.lower-price-order'));
                 $('.product-template__submit').after($('.quick-order'));
                 $('.quick-order, .lower-price-order').wrapAll('<div class="order-options"/>');
             }
             if ($('.quantity')) {
                 $('.quantity').each(function () {
                     $(this).prev().prepend($(this));
                 });
             }
             isCompleted = true;
         } else if (!isCompleted && !isMobile) {}
         //Functions
         function checkArrayItemValue(arrayIndex, itemNode) {
             if (checkedInputsArray[arrayIndex] > 0) {
                 itemNode.closest('.filter-item__wrapper').prev().addClass('filter-item__header_chosen');
             } else if (checkedInputsArray[arrayIndex] == 0) {
                 itemNode.closest('.filter-item__wrapper').prev().removeClass('filter-item__header_chosen');
                 itemNode.closest('.filter-item__wrapper').prev().find('.filter-item__name span').text('');
             }
         }

         function changeActiveClass(selectorClass) {
             var selectorEl = document.querySelectorAll(selectorClass);
             for (var i = 0; i < selectorEl.length; i++) {
                 selectorEl[i].addEventListener('click', function (e) {
                     e.preventDefault();
                     for (var j = 0; j < selectorEl.length; j++) {
                         selectorEl[j].classList.remove(selectorClass.replace('.', '') + '_active');
                     }
                     this.classList.add(selectorClass.replace('.', '') + '_active');
                 });
             }
         }

         function createInnerCarousel() {
             if ($('.inner-carousel')) {
                 $('.inner-carousel').addClass('swiper-container').children().toggleClass('row no-gutters swiper-wrapper');
                 $('.inner-carousel').find('.box__col').toggleClass('box__col col-2 swiper-slide inner-carousel__item');
                 $('.inner-carousel').each(function (index) {
                     $(this).addClass('inner-carousel-' + index);
                     $(this).next().find('.inner-carousel-navigation__btn-prev').addClass('inner-carousel-navigation__btn-prev-' + index);
                     $(this).next().find('.inner-carousel-navigation__btn-next').addClass('inner-carousel-navigation__btn-next-' + index);
                     var innerCarouselItem = new Swiper('.inner-carousel-' + index, {
                         slidesPerView: 'auto',
                         loop: true,
                         freeMode: true,
                         navigation: {
                             nextEl: '.inner-carousel-navigation__btn-next-' + index,
                             prevEl: '.inner-carousel-navigation__btn-prev-' + index
                         },
                         breakpoints: {
                             // when window width is <= 320px
                             320: {
                                 slidesPerView: 'auto'
                             },
                             // when window width is <= 768px
                             576: {
                                 slidesPerView: 'auto'
                             },
                             // when window width is <= 768px
                             768: {
                                 slidesPerView: 2
                             },
                             // when window width is <= 992px
                             992: {
                                 slidesPerView: 3
                             }
                         }
                     });
                     innerCarousel.push(innerCarouselItem);
                 });
             }
         }

         function destroyInnerCarousel() {
             for (var i = 0; i < innerCarousel.length; i++) {
                 $('.inner-carousel').removeClass('inner-carousel-' + i);
                 innerCarousel[i].destroy();
             }
             $('.inner-carousel').removeClass('swiper-container').children().toggleClass('row no-gutters swiper-wrapper');
             $('.inner-carousel').find('.inner-carousel__item').toggleClass('box__col col-2 swiper-slide inner-carousel__item');
         }

         function createFilterCarousel() {
             if ($('.filter-item').length !== 0) {
                 $('.filter-item').addClass('swiper-slide filter__carousel-item').wrapAll('<div class="swiper-container filter__carousel"><div class="swiper-wrapper"/>');
                 filterCarousel = new Swiper('.filter__carousel', {
                     slidesPerView: 'auto',
                     freeMode: true
                 });
             }
         }

         function destroyFilterCarousel() {
             if ($('.filter-item').length !== 0) {
                 $('.filter-item').removeClass('swiper-slide filter__carousel-item').unwrap().unwrap();
                 filterCarousel.destroy();
             }
         }

         function createMSwiperSlider() {
             $('.m-carousel').addClass('swiper-container');
             $('.m-carousel > div').toggleClass('row align-items-stretch no-gutters swiper-wrapper');
             $('.m-carousel > div > div').toggleClass('col-4 swiper-slide m-carousel__item');
             $('.m-carousel').each(function (index) {
                 $(this).addClass('m-carousel-' + index).next().addClass('m-carousel-navigation-' + index);
                 $(this).next().find('.m-carousel-navigation__btn-prev').addClass('m-carousel-navigation__btn-prev-' + index);
                 $(this).next().find('.m-carousel-navigation__btn-next').addClass('m-carousel-navigation__btn-next-' + index);
                 var mCarouselItem = new Swiper('.m-carousel-' + index, {
                     slidesPerView: 'auto',
                     freeMode: true,
                     loop: true,
                     navigation: {
                         nextEl: '.m-carousel-navigation__btn-next-' + index,
                         prevEl: '.m-carousel-navigation__btn-prev-' + index
                     },
                     breakpoints: {
                         // when window width is <= 320px
                         320: {
                             slidesPerView: 'auto'
                         },
                         // when window width is <= 768px
                         576: {
                             slidesPerView: 'auto'
                         },
                         // when window width is <= 768px
                         768: {
                             slidesPerView: 2
                         },
                         // when window width is <= 992px
                         992: {
                             slidesPerView: 3
                         }
                     }
                 });
                 mCarousel.push(mCarouselItem);
             });
         }

         function destroyMSwiperSlider() {
             for (var i = 0; i < mCarousel.length; i++) {
                 $('.m-carousel').removeClass('m-carousel-' + i);
                 mCarousel[i].destroy();
             }
             $('.m-carousel').removeClass('swiper-container');
             $('.m-carousel > div').toggleClass('row align-items-stretch no-gutters swiper-wrapper');
             $('.m-carousel > div > div').toggleClass('col-4 swiper-slide m-carousel__item');
         }

         function getDeviceType() {
             if (window.innerWidth < 992) {
                 isMobile = true;
             } else if (window.innerWidth > 991) {
                 isMobile = false;
             }
         }

         function changeOpenClass(selectorClass, changeElClass) {
             var selectorEl = document.querySelector(selectorClass);
             if (selectorEl) {
                 selectorEl.addEventListener('click', function () {
                     selectorEl.nextElementSibling.classList.toggle(changeElClass + '_opened');
                 });
             }
         }

         function createProductSizeCarousel() {
             if ($('.product-size').length !== 0) {
                 $('.product-size').addClass('swiper-container');
                 $('.product-size__item').wrap('<div class="swiper-slide product-size__slide"/>');
                 $('.product-size__slide').wrapAll('<div class="swiper-wrapper"/>');
                 productSizeCarousel = new Swiper('.product-size', {
                     slidesPerView: 'auto',
                     freeMode: true,
                     spaceBetween: 15
                 });
             }
         }

         function destroyProductSizeCarousel() {
             if ($('.product-size').length !== 0) {
                 productSizeCarousel.destroy();
                 $('.product-size__item').unwrap().unwrap();
                 $('.product-size').removeClass('swiper-container');
             }
         }

         function getDataValue(selectorEl) {
             var dataValue = selectorEl.getAttribute('data-full-img');
             return dataValue;
         }
         //Option
         $('.custom-checkbox__input').on('click', function () {
             var checked;
             if ($(this).is(':checked')) {
                 $('.user-option__popup').addClass('popup_opened');
             } else {
                 $('.user-option__popup').removeClass('popup_opened');
             }
         });
         //Popular
         var testBlock = '<a href="#" class="popular-item popular__col">' +
             '<svg width="88" height="40" viewBox="0 0 88 40" fill="none" xmlns="http://www.w3.org/2000/svg">' +
             '<path d="M6.27936 40C6.23903 40 6.11795 40 5.99695 39.9597C4.90797 39.8387 3.77867 39.395 2.89135 38.7497C1.56037 37.7817 0.592401 36.3701 0.189073 34.7568C-0.0125905 33.9098 -0.052943 32.9418 0.0680553 32.0544C0.43105 29.6748 2.00401 27.6179 4.18198 26.6902C4.74663 26.4482 5.27098 26.3272 5.9163 26.2466C6.2793 26.2062 7.12635 26.2062 7.48934 26.2466C8.25566 26.3676 8.90093 26.5289 9.54625 26.8515C10.2722 27.2145 10.7562 27.5775 11.3612 28.1825C11.8856 28.7068 12.2083 29.1505 12.6116 29.8361L12.6922 29.9975L12.4099 30.1991C12.0066 30.4815 11.0789 31.1671 10.7966 31.3688L10.5546 31.5301L10.4336 31.2882C10.3126 31.0058 10.2319 30.8445 10.0706 30.6428C9.50594 29.7555 8.53793 29.1505 7.52961 28.9488C7.00529 28.8278 6.31961 28.8682 5.79529 28.9892C5.23063 29.1102 4.62564 29.3925 4.18198 29.7555C3.98031 29.9168 3.65767 30.2395 3.49634 30.4411C2.68968 31.4494 2.36708 32.7401 2.64941 34.0308C2.73008 34.4744 3.01241 35.0794 3.2544 35.4424C3.94006 36.491 4.98863 37.1364 6.19861 37.2977C6.40028 37.338 6.92462 37.338 7.12629 37.2977C8.17494 37.1767 9.18324 36.6121 9.82856 35.7651C10.0302 35.5231 10.1513 35.3214 10.3126 34.9987C10.3933 34.8777 10.4336 34.7568 10.4336 34.7568C10.4336 34.7568 10.9176 35.1198 11.5226 35.5231L12.6116 36.2894L12.4906 36.491C12.2486 36.9347 11.8856 37.459 11.5226 37.822C10.9176 38.4674 10.3126 38.911 9.54625 39.3143C8.8606 39.637 8.17492 39.8387 7.4086 39.9597C7.2876 40 6.40036 40 6.27936 40ZM15.4349 33.0224V26.3272H16.7255H18.0161V28.7472V31.1671H21.4041H24.7921V28.7472V26.3272H26.0827H27.3733V33.0224V39.7177H26.0827H24.7921V36.7734V33.8291H21.4041H18.0161V36.7734V39.7177H16.7255H15.4349V33.0224ZM30.0757 38.548C30.358 37.9027 31.689 34.8777 33.0603 31.8528L35.5206 26.3272H37.3356H39.1506L42.0948 32.9418C43.7081 36.5717 45.0794 39.5967 45.0794 39.637L45.1197 39.7177H43.5468H41.9738L41.3689 38.3867L40.7639 37.0557H37.2952H33.8265L33.2216 38.3867L32.6166 39.7177H31.0436H29.4707L30.0757 38.548ZM39.7152 34.3938C39.7152 34.3534 37.3356 29.0699 37.3356 29.0699C37.3356 29.0699 35.0769 34.3131 35.0365 34.3938C35.0365 34.3938 35.9239 34.3938 37.3759 34.3938C39.3522 34.3938 39.7152 34.3938 39.7152 34.3938ZM47.2171 33.0224V26.3272H49.0321H50.847L53.7106 30.3605L56.5743 34.3938V30.3605V26.3272H57.8649H59.1555V33.0224V39.7177H58.1069H57.0583L53.4283 34.6761C51.4117 31.8931 49.7984 29.6345 49.7984 29.6345C49.7984 29.6345 49.7984 31.8931 49.7984 34.6761V39.7177H48.5077H47.2171V33.0224ZM63.9552 33.0224V26.3272H68.6337H73.3124V27.6582V28.9892H69.9244H66.5365V30.3202V31.6511H69.6421H72.7477V32.9821V34.3131H69.6421H66.5365V35.6441V36.9751H70.4084H74.2803V38.306V39.637H69.1177H63.9552V33.0224ZM77.9506 33.0224V26.3272H79.2413H80.5319V31.6915V37.0557H84.1618H87.7918V38.3867V39.7177H82.8308H77.87V33.0224H77.9506Z" fill="#34323A"></path>' +
             '<path d="M38.8911 19.7265C37.5687 19.6684 36.0739 19.2613 34.8857 18.6215C33.9658 18.1368 33.2376 17.5939 32.4901 16.8185C31.896 16.2174 31.4552 15.6552 31.0911 15.0348L30.9186 14.7634L31.1486 14.6471C31.6086 14.4144 33.6592 13.3481 33.6783 13.3481C33.6783 13.3481 33.7742 13.4645 33.87 13.6002C34.9432 15.2093 36.6105 16.2562 38.5653 16.547C38.9103 16.6052 39.9451 16.5858 40.3093 16.5276C40.7309 16.4695 41.2292 16.3532 41.5358 16.2368C41.555 16.2368 41.5166 16.1593 41.4208 16.0623C40.7117 15.2287 40.0793 14.0849 39.7152 13.0186C39.0636 11.0798 39.0253 9.02479 39.581 7.06667C39.926 5.86466 40.5393 4.68202 41.2867 3.73205C41.4017 3.59633 41.4975 3.46062 41.4975 3.46062C41.4975 3.42184 40.9034 3.26676 40.5201 3.18922C39.926 3.09228 38.9678 3.07287 38.4312 3.16981C37.5113 3.32491 36.5913 3.67388 35.8631 4.13917C35.0582 4.66263 34.3874 5.32181 33.8317 6.13607L33.6016 6.50444L33.295 6.34933C33.1226 6.25239 32.5093 5.9422 31.9152 5.63201C31.3211 5.32181 30.842 5.06977 30.842 5.06977C30.842 5.06977 31.2061 4.46878 31.3594 4.25552C31.8194 3.57696 32.6434 2.70451 33.2375 2.20045C34.8282 0.920886 36.5722 0.222954 38.5653 0.0290809C38.9294 -0.00969364 39.8493 -0.00969364 40.1943 0.0290809C41.4592 0.145405 42.5324 0.455596 43.6439 1.01783L44.1805 1.28925L44.353 1.19232C44.6405 1.01783 45.1579 0.78518 45.5412 0.630082C46.6336 0.203562 47.726 0.00969837 48.9333 0.00969837C49.7766 0.00969837 50.4857 0.0872522 51.2523 0.281125C52.9771 0.707645 54.4719 1.5413 55.7368 2.80147C56.4267 3.48003 56.9633 4.17796 57.4041 4.95345L57.4616 5.06977L56.3884 5.63201C55.7943 5.9422 55.181 6.27178 55.0086 6.34933L54.7019 6.50444L54.4719 6.15545C53.3604 4.44937 51.5206 3.3443 49.5083 3.15043C48.9142 3.09227 48.0326 3.15042 47.496 3.26675C47.2277 3.32491 46.7678 3.46064 46.7678 3.48003C46.7678 3.48003 46.8444 3.59634 46.921 3.69328C48.0709 5.12794 48.78 6.8534 49.01 8.77274C49.0675 9.23803 49.0675 10.4013 49.0292 10.8472C48.8183 12.7665 48.1093 14.5308 46.9594 15.946C46.6911 16.2756 46.7102 16.2369 46.7869 16.2756C46.8252 16.295 47.0169 16.3338 47.1894 16.3919C48.1284 16.6246 49.1442 16.6634 50.1024 16.4889C51.8272 16.1593 53.3987 15.1318 54.3952 13.6389C54.4911 13.5032 54.5677 13.3869 54.5869 13.3675C54.6061 13.3481 54.8361 13.4645 55.1044 13.6002C55.641 13.8716 57.3275 14.7634 57.3466 14.7828C57.385 14.8216 56.8292 15.6164 56.5034 16.043C56.1967 16.4501 55.3726 17.2644 54.9702 17.5939C53.552 18.7378 51.9614 19.4164 50.2174 19.6684C48.2051 19.9592 46.0778 19.5715 44.353 18.6215L44.1039 18.4858L43.7973 18.6409C42.3791 19.4164 40.6542 19.8041 38.8911 19.7265ZM44.5063 14.1236C45.3496 13.0767 45.867 11.7972 46.0012 10.4982C46.0395 10.0523 46.0203 9.16048 45.9437 8.77274C45.752 7.66766 45.3687 6.73707 44.7171 5.88403C44.6021 5.72893 44.4297 5.49628 44.3338 5.39935L44.1614 5.20547L44.008 5.37996C43.7972 5.61261 43.4523 6.0779 43.2798 6.36871C42.379 7.82276 42.0916 9.62579 42.4749 11.3319C42.7048 12.3982 43.2606 13.4839 43.9889 14.3369L44.1614 14.5308L44.238 14.4532C44.2763 14.4144 44.4105 14.2593 44.5063 14.1236Z" fill="#34323A"></path>' +
             '</svg>' +
             '</a>';
         $('.popular__btn').on('click', function (e) {
             e.preventDefault();
             $(this).before(testBlock);
         });
         //Stars
         /* 1. Visualizing things on Hover - See next part for action on click */
         $('.stars__grid li').on('mouseover', function () {
             var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
             // Now highlight all the stars that's not after the current hovered star
             $(this).parent().children('li.stars__item').each(function (e) {
                 if (e < onStar) {
                     $(this).addClass('hover');
                 } else {
                     $(this).removeClass('hover');
                 }
             });
         }).on('mouseout', function () {
             $(this).parent().children('li.stars__item').each(function (e) {
                 $(this).removeClass('hover');
             });
         });
         /* 2. Action to perform on click */
         $('.stars__grid li').on('click', function () {
             var onStar = parseInt($(this).data('value'), 10); // The star currently selected
             var stars = $(this).parent().children('li.stars__item');
             for (i = 0; i < stars.length; i++) {
                 $(stars[i]).removeClass('selected');
             }
             for (i = 0; i < onStar; i++) {
                 $(stars[i]).addClass('selected');
             }
             // JUST RESPONSE (Not needed)
             var ratingValue = parseInt($('.stars__grid li.selected').last().data('value'), 10);
             if (ratingValue) {
                 $('.stars__value').text(ratingValue + '/5');
             }
         });
         //Favorite
         var favoriteEl = document.querySelectorAll('.to-favorite');
         for (var i = 0; i < favoriteEl.length; i++) {
             favoriteEl[i].addEventListener('click', function () {
                 this.classList.toggle('to-favorite_active');
             });
         }
         //Call-back
         var callBackBtn = document.querySelector('.header__w-time-btn');
         var callBackEl = document.querySelector('.call-back');
         if (callBackBtn) {
             callBackBtn.addEventListener('click', function () {
                 callBackEl.classList.toggle('popup_opened');
             });
         }
         //Popup
         var closePopupSelector = document.querySelectorAll('.popup__close-btn');
         for (var i = 0; i < closePopupSelector.length; i++) {
             closePopupSelector[i].addEventListener('click', function (e) {
                 this.parentNode.classList.remove('popup_opened');
             });
         }
         //Full brands
         var fullBrandsLink = document.querySelector('.menu__link_s');
         if (fullBrandsLink) {
             var fullBrandsClostBtn = document.querySelector('.full-brands__close-btn');
             fullBrandsLink.addEventListener('click', function (e) {
                 e.preventDefault();
                 this.classList.toggle('menu__link_s-active');
                 document.querySelector('.header').classList.toggle('header_s');
                 document.querySelector('.full-brands').classList.toggle('full-brands_opened');
             });
             fullBrandsClostBtn.addEventListener('click', function (e) {
                 e.preventDefault();
                 document.querySelector('.full-brands').classList.remove('full-brands_opened');
                 document.querySelector('.menu__link_s').classList.remove('menu__link_s-active');
             });
         }
         //Sort
         changeActiveClass('.sort__item');
         //Filter
         if ($('.filter-item__header-wrapper')) {
             $('.filter-item__header-wrapper').on('click', function () {
                 if ($(this).parent().next().hasClass('filter-item__wrapper_opened')) {
                     $(this).removeClass('filter-item__header-wrapper_active');
                     $(this).parent().next().removeClass('filter-item__wrapper_opened');
                     if (!$('.filter-item__wrapper').hasClass('filter-item__wrapper_opened')) {
                         $('.filter').removeClass('filter_active');
                     }
                 } else {
                     $('.filter-item__header-wrapper').removeClass('filter-item__header-wrapper_active');
                     $(this).addClass('filter-item__header-wrapper_active');
                     $('.filter-item__wrapper').removeClass('filter-item__wrapper_opened');
                     $(this).parent().next().addClass('filter-item__wrapper_opened');
                     if ($('.filter-item__wrapper').hasClass('filter-item__wrapper_opened')) {
                         $('.filter').addClass('filter_active');
                     }
                 }
             });
         }
         //Product favorite
         var toFavoriteEl = document.querySelector('.product-template__favorite');
         if (toFavoriteEl) {
             toFavoriteEl.addEventListener('click', function () {
                 this.classList.toggle('product-template__favorite_active');
             });
         }
         //Quick order
         var quickOrderBtn = document.querySelector('.quick-order__btn');
         if (quickOrderBtn) {
             quickOrderBtn.addEventListener('click', function () {
                 this.nextElementSibling.classList.toggle('popup_opened');
             });
         }
         //Product size
         changeActiveClass('.product-size__item');
         //Lower price
         //Quick order
         var lowerPriceBtn = document.querySelector('.product-price__lower-btn');
         if (lowerPriceBtn) {
             lowerPriceBtn.addEventListener('click', function () {
                 this.nextElementSibling.classList.toggle('popup_opened');
             });
         }
         //Tabs
         var tabsBtnEl = document.querySelectorAll('.js-tab-btn');
         var tabsSectionEl = document.querySelectorAll('.js-tab-section');
         if (tabsBtnEl) {
             for (var i = 0; i < tabsBtnEl.length; i++) {
                 (function (index) {
                     tabsBtnEl[i].addEventListener('click', function () {
                         for (var j = 0; j < tabsBtnEl.length; j++) {
                             tabsBtnEl[j].classList.remove('js-tab-btn_active');
                             tabsSectionEl[j].classList.remove('js-tab-section_active');
                         }
                         this.classList.add('js-tab-btn_active');
                         tabsSectionEl[index].classList.add('js-tab-section_active');
                     });
                 })(i);
             }
         }
         //Testimonail form
         var testimonialAddBtn = document.querySelector('.testimonials__add-btn');
         if (testimonialAddBtn) {
             testimonialAddBtn.addEventListener('click', function () {
                 this.nextElementSibling.classList.toggle('testimonials__form_opened');
             });
         }
         //Comment
         changeOpenClass('.basket-template__form-add-comment', 'basket-template__form-comment');
         //Custom scrollbar
         var scrollbarEl = document.querySelectorAll('.custom-scrollbar');
         if (scrollbarEl) {
             for (var i = 0; i < scrollbarEl.length; i++) {
                 const simpleBar = new SimpleBar(scrollbarEl[i], {
                     autoHide: false
                 });
                 simpleBar.getContentElement();
             }
         }
         //Letter
         var letterEl = document.querySelectorAll('.letter');
         var letterListEl = document.querySelector('.brands__letter-list');
         var letterListCloseEl = document.querySelector('.brands__letter-list-close-btn');
         for (var i = 0; i < letterEl.length; i++) {
             letterEl[i].addEventListener('click', function () {
                 if (this.classList.contains('letter_active')) {
                     letterListEl.classList.remove('brands__letter-list_opened');
                     this.classList.remove('letter_active');
                 } else {
                     for (var j = 0; j < letterEl.length; j++) {
                         letterEl[j].classList.remove('letter_active');
                     }
                     letterListEl.classList.remove('brands__letter-list_opened');
                     this.classList.add('letter_active');
                     setTimeout(function () {
                         letterListEl.classList.add('brands__letter-list_opened');
                     }, 150);
                 }
             });
         }
         letterListCloseEl.addEventListener('click', function () {
             for (var i = 0; i < letterEl.length; i++) {
                 letterEl[i].classList.remove('letter_active');
             }
             letterListEl.classList.remove('brands__letter-list_opened');
         });
         $('.complex-letter').on('click', function () {
             $(this).toggleClass('complex-letter_active').next().toggleClass('brands__letters-list_opened');
         });
         //Media
         $(window).on('scroll', function () {
             if ($(window).scrollTop() >= ($('main').offset().top + $('main').height() / 2)) {
                 $('.to-top').addClass('to-top_actived');
             } else {
                 $('.to-top').removeClass('to-top_actived');
             }
         });
         $(window).on('resize', function () {
             getDeviceType();
             if (!isMobile && isCompleted) {
                 $('.footer__bottom > .row').prepend($('.footer__top > .row .col-12:nth-child(3)'));
                 $('.footer__bottom > .row > .col-12:last-child > .row').prepend($('.footer__top > .row .col-12:nth-child(2)'));
                 $('.footer__bottom > .row > .col-12:last-child > .row').prepend($('.footer__top > .row .col-12:first-child'));
                 $('.top-menu').append($('.m-menu .m-menu__link:nth-child(n+7)'));
                 $('.top-menu a').toggleClass('m-menu__link top-menu__link');
                 $('.menu__wrapper').append($('.m-menu .m-menu__link'));
                 $('.menu__wrapper a').toggleClass('m-menu__link menu__link');
                 $('.menu__wrapper .menu__link:first-child').addClass('menu__link_s');
                 $('.header__top .col-xl-4').append($('.m-menu .user-option'));
                 if ($('.product-gallery')) {
                     $('.product-template__note').before($('.product-gallery'));
                 }
                 $('.catalog-template__list .box__col.col-12').each(function (index) {
                     if (index % 2 !== 0) {
                         $(this).parent().parent().before($(this));
                     } else {
                         $(this).parent().parent().after($(this));
                     }
                 });
                 if ($('.about-brand__title')) {
                     $('.about-brand__content').prepend($('.about-brand__title'));
                 }
                 if ($('.tabs')) {
                     $('.js-tab-section').each(function (index) {
                         $('.tabs__grid').append($(this));
                     });
                 }
                 if ($('.quantity')) {
                     $('.quantity').each(function () {
                         $(this).parent().after($(this));
                     });
                 }
                 $('.search__input').attr('placeholder', 'Начните вводить название бренда или товара, а мы уж подскажем ;)');
                 $('.brands .col-12').append($('.brands__grid'));
                 $('.brands__letters-list').removeClass('custom-scrollbar');
                 destroyMSwiperSlider();
                 destroyFilterCarousel();
                 destroyProductSizeCarousel();
                 destroyInnerCarousel();
                 isCompleted = false;
             } else if (!isCompleted && isMobile) {
                 $('.footer__top > .row').prepend($('.footer__bottom > .row > .col-12:last-child .col-lg-6:nth-child(2)'));
                 $('.footer__top > .row').prepend($('.footer__bottom > .row > .col-12:last-child .col-lg-6:first-child'));
                 $('.footer__top > .row .col-12:nth-child(2)').after($('.footer__bottom > .row > .col-12:first-child'));
                 $('.m-menu__grid').append($('.menu__wrapper a'));
                 $('.m-menu__grid').append($('.top-menu a'));
                 $('.m-menu__grid').append($('.user-option'));
                 $('.m-menu__grid a').removeClass('top-menu__link');
                 $('.m-menu__grid a').removeClass('menu__link');
                 $('.m-menu__grid a').removeClass('menu__link_s');
                 $('.m-menu__grid a').addClass('m-menu__link');
                 $('.brands__letters-list').addClass('custom-scrollbar');
                 lettersSimpleBar = new SimpleBar(document.querySelector('.brands__letters-list.custom-scrollbar'), {
                    autoHide: false
                });
                lettersSimpleBar.getContentElement();
                 if ($('.product-options')) {
                     $('.product-options').after($('.product-gallery'));
                 }
                 $('.header__options').append($('.brands__grid'));
                 $('.catalog-template__list .box__col.col-12').each(function (index) {
                     if (index % 2 !== 0) {
                         $(this).next().find('.box__col.col-6:nth-child(4)').after($(this));
                     } else {
                         $(this).prev().find('.box__col.col-6:nth-child(4)').after($(this));
                     }
                 });
                 if ($('.about-brand__title')) {
                     $('.about-brand > .row > .col-12:first-child').prepend($('.about-brand__title'));
                 }
                 if ($('.tabs')) {
                     $('.js-tab-section').each(function (index) {
                         $('.js-tab-btn').eq(index).after($(this));
                     });
                 }
                 if ($('.quantity')) {
                     $('.quantity').each(function () {
                         $(this).prev().prepend($(this));
                     });
                 }
                 $('.search__input').attr('placeholder', 'Начните вводить бренд');
                 createMSwiperSlider();
                 createProductSizeCarousel();
                 createFilterCarousel();
                 createInnerCarousel();
                 isCompleted = true;
             }
         });
         //Show more
         var showMoreBtnEl = document.querySelector('.show-more');
         var showMoreTextEl = document.querySelector('.show-more-text');
         if (showMoreBtnEl && showMoreTextEl) {
             showMoreBtnEl.addEventListener('click', function () {
                 this.classList.toggle('show-more_active');
                 showMoreTextEl.classList.toggle('show-more-text_opened');
                 if (this.classList.contains('show-more_active')) {
                     this.firstElementChild.textContent = 'Свернуть';
                 } else {
                     this.firstElementChild.textContent = 'Подробнее';
                 }
             });
         }
     });
     //Smooth scroll
     // Select all links with hashes
     $('a[href*="#"]')
         // Remove links that don't actually link to anything
         .not('[href="#"]')
         .not('[href="#0"]')
         .click(function (event) {
             // On-page links
             if (
                 location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                 location.hostname == this.hostname
             ) {
                 // Figure out element to scroll to
                 var target = $(this.hash);
                 target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                 // Does a scroll target exist?
                 if (target.length) {
                     // Only prevent default if animation is actually gonna happen
                     event.preventDefault();
                     $('html, body').animate({
                         scrollTop: target.offset().top - 56
                     }, 1000, function () {
                         // Callback after animation
                         // Must change focus!
                         var $target = $(target);
                         $target.focus();
                         if ($target.is(":focus")) { // Checking if the target was focused
                             return false;
                         } else {
                             //  $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                             $target.focus(); // Set focus again
                         };
                     });
                 }
             }
         });
 }();