'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const debug = require('gulp-debug');
const gulpIf = require('gulp-if');
const clean = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const rev = require('gulp-rev');
const revReplace = require('gulp-rev-replace');
const browserSync = require('browser-sync').create();
const del = require('del');
const combine = require('stream-combiner2').obj;

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

sass.compiler = require('node-sass');

gulp.task('styles', function(){
    return gulp.src('app/sass/*.scss')
    .pipe(gulpIf(isDevelopment, sourcemaps.init())) //file.sourceMap
    .pipe(combine(sass(), concat('styles.css')))
    .pipe(gulpIf(!isDevelopment,combine(clean(), rev())))
    .pipe(gulpIf(isDevelopment,sourcemaps.write())) //file.sourceMap
    .pipe(gulp.dest('public/css/'))
    .pipe(gulpIf(!isDevelopment,combine(rev.manifest('css.json'),gulp.dest('manifest'))))
});

gulp.task('scripts', function(){
    return gulp.src(['app/js/plugins.js', 'app/js/main.js'])
    .pipe(gulpIf(isDevelopment, sourcemaps.init())) //file.sourceMap
    .pipe(concat('scripts.js'))
    .pipe(gulpIf(!isDevelopment, combine(uglify(), rev())))
    .pipe(gulpIf(isDevelopment,sourcemaps.write())) //file.sourceMap
    .pipe(gulp.dest('public/js/'))
    .pipe(gulpIf(!isDevelopment,combine(rev.manifest('js.json'),gulp.dest('manifest'))))
});

gulp.task('clean', function(){
    return del('public');
});

gulp.task('assets', function(){
    return gulp.src('app/assets/**', {since: gulp.lastRun('assets')})
        .pipe( gulpIf( !isDevelopment, revReplace({manifest: gulp.src('manifest/css.json')}) ) )
        .pipe( gulpIf( !isDevelopment, revReplace({manifest: gulp.src('manifest/js.json')}) ) )
        .pipe(gulp.dest('public'))
});

gulp.task('build', gulp.series('clean', gulp.parallel('styles', 'scripts', 'assets')));

gulp.task('watch', function(){
    gulp.watch('app/sass/**/*.*', gulp.series('styles'));
    gulp.watch('app/js/**/*.*', gulp.series('scripts'));
    gulp.watch('app/assets/**/*.*', gulp.series('assets'));
});

gulp.task('serve', function(){
    browserSync.init({
        server: 'public'
    });
    browserSync.watch('public/**/*.*').on('change', browserSync.reload);
});

gulp.task('dev', gulp.series('build', gulp.parallel('watch', 'serve')));